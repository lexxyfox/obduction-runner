#!/bin/bash

# Automatically sets up wine prefix if needed and runs Obduction without Steam or any distractions.
# Specifically: hides WINE dialog, isolates home folder, installs DXVK, and installs WMF.
# Requires Obduction to be installed in "$GamePath" (defaults to Steam install)
# Requires WINE to be installed in "$WINEVERPATH" (defaults to wine "lutris-fshack-7.2" from lutris)
# "splash" source: https://e926.net/posts/925516 original creator "mmabelpines" with modification
# DXVK source: https://github.com/doitsujin/dxvk original creator "doitsujin"
# The following original code is copyrighted by me

DirName="$(dirname -- "$(readlink -f -- "$0")")"

: "${HOME:=/home/$USER}"
: "${STEAM_COMPAT_CLIENT_INSTALL_PATH:=$HOME/.steam/steam}"
: "${GamePath:=$(readlink -f "$STEAM_COMPAT_CLIENT_INSTALL_PATH/steamapps/common/Obduction")}"
: "${XDG_DATA_HOME:=$HOME/.local/share}"
: "${WINEVERPATH:=$XDG_DATA_HOME/lutris/runners/wine/lutris-fshack-7.2-x86_64}"

export DXVK_LOG_LEVEL="${DXVK_LOG_LEVEL:-warn}"
export DXVK_LOG_PATH="${DXVK_LOG_PATH:-none}"
export PATH="$WINEVERPATH/bin:$PATH"
export USER=Public
export WINEARCH='win64'
export WINEDEBUG="${WINEDEBUG:--all}"
export WINEDLLPATH="${WINEDLLPATH:-$WINEVERPATH/lib/wine}"
export WINEESYNC="${WINEESYNC:-1}"
export WINEFSYNC="${WINEFSYNC:-1}"
export WINELOADER="${WINELOADER:-$WINEVERPATH/bin/wine}"
export WINEPREFIX="${WINEPREFIX:-$DirName/pfx}"
export WINESERVER="${WINESERVER:-$WINEVERPATH/bin/wineserver}"

set -uex

if test ! -d "$WINEPREFIX"; then
  winex() {
    wine "$@" && wine64 "$@"
  }
  DISPLAY= wineboot -i
  #W_WINDIR_UNIX="$(readlink -f -- "$(DISPLAY= wine winepath -u 'c:/windows')")"
  tar -xvJC "$WINEPREFIX/drive_c" < .txz &
  winex regedit .ini &
  (
    cd "$WINEPREFIX/drive_c/users/$USER"
    rm -f Desktop Documents Downloads Music Pictures Videos
    mkdir -p Desktop Documents Downloads Music Pictures Videos
  ) &
  echo disable > "$WINEPREFIX/.update-timestamp" &
  wait
  wineserver --wait # important!
  winex regsvr32 colorcnv msmpeg2adec msmpeg2vdec
fi

mv -f "$GamePath/Obduction/Content/Splash/Splash.bmp" "$GamePath/Obduction/Content/Splash/Splash.bak" || true
mv -f "$GamePath/Obduction/Content/Movies/UE4_Logo.mp4" "$GamePath/Obduction/Content/Movies/UE4_Logo.bak" || true
mv -f "$GamePath/Obduction/Content/Movies/Cyan_Logo.mp4" "$GamePath/Obduction/Content/Movies/Cyan_Logo.bak" || true
cp --dereference --reflink=auto --remove-destination "$DirName/splash" "$GamePath/Obduction/Content/Splash/Splash.bmp"
gamemoderun wine "$GamePath/Obduction.exe"
mv -f "$GamePath/Obduction/Content/Movies/Cyan_Logo.bak" "$GamePath/Obduction/Content/Movies/Cyan_Logo.mp4"
mv -f "$GamePath/Obduction/Content/Movies/UE4_Logo.bak" "$GamePath/Obduction/Content/Movies/UE4_Logo.mp4"
mv -f "$GamePath/Obduction/Content/Splash/Splash.bak" "$GamePath/Obduction/Content/Splash/Splash.bmp"
